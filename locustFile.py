from locust import HttpUser, task


# Test de performance d'utilisateurs connectés en même temps
class LocustTest(HttpUser):
    
    @task
    def index(self):
        self.client.get(url="/")
        self.client.post("/showSummary", data={"email": "mauvaise@adresse.com"})
        self.client.post("/showSummary", data={"email": "megadmin@test.fr"})
        self.client.get(url="/book/Event/Megadmin")
        self.client.post("/purchasePlaces", data=dict(competition="MegaEvent", club="Megadmin", places="5"))