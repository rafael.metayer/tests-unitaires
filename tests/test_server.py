import pytest
from server import app

# commande : pytest --disable-warnings -v test_server.py


# Configuration du server
@pytest.fixture
def tester():
    app.config["TESTING"] = True
    with app.test_client() as tester:
        print('=> *** I am a fixture : WebApp was successfully created and the test was : ')
        yield tester


# Test d'arrivée sur la page de connexion
def test_home_page_returns_correct_html(tester):
    response = tester.get('/')
    assert response.status_code == 200
    assert b'<input' in response.data


# Test de connexion réussie
def test_good_user_login(tester):
    rv = tester.post("/showSummary", data=dict(email="john@simplylift.co"), follow_redirects=True)
    assert rv.status_code == 200
    data = rv.data.decode()
    assert "Welcome, john@simplylift.co" in data


# Test de connexion echouée avec une mauvaise adresse mail
def test_wrong_user_login(tester):
    rv = tester.post("/showSummary", data=dict(email="mauvaise@adresse.com"), follow_redirects=True)
    assert rv.status_code == 200
    print(rv.data.decode())
    assert "Désolé, cet email n&#39;a pas été trouvé" in rv.data.decode()


# Test de visualisation d'un évènement valide
def test_booking_valid(tester):
    rv = tester.get("http://127.0.0.1:5000/book/Event/Megadmin")
    assert rv.status_code == 200
    assert "Event" in rv.data.decode()


# Test de visualisation d'un évènement invalide
def test_booking_invalid(tester):
    response = tester.get("http://127.0.0.1:5000/book/Spring%20Festival/Megadmin", follow_redirects=True)
    assert response.status_code == 200
    assert "Lien incorrect, veuillez vous reconnecter" in response.data.decode()


# Test d'achat de places d'un évènement valide
def test_purchase_places(tester):
    rv = tester.post("/purchasePlaces", data=dict(competition="Event", club="She Lifts", places="1"),
                     follow_redirects=True)
    assert rv.status_code == 200
    data = rv.data.decode()
    print(data)
    assert "Points available: 9" in data
    assert """<li>
                    Event </br>
                    Date: 2024-10-22 13:30:00 </br>
                    Number of Places: 12 </br>
    """ in data


# Test d'achat de plus de 12 places
def test_purchase_more_12_places(tester):
    rv = tester.post("/purchasePlaces", data=dict(competition="Event", club="She Lifts", places="13"),
                     follow_redirects=True)
    assert rv.status_code == 200
    data = rv.data.decode()
    assert "Nombre de places invalides !" in data


# Test d'achat d'un utilisateur n'ayant pas assez de points
def test_purchase_user_4_places(tester):
    rv = tester.post("/purchasePlaces", data=dict(competition="Event", club="Iron Temple", places="5"),
                     follow_redirects=True)
    assert rv.status_code == 200
    data = rv.data.decode()
    assert "Pas assez de points disponibles dans le club!" in data


# Test de déconnexion
def test_logout(tester):
    rv1 = tester.get("/logout", follow_redirects=True)
    assert rv1.status_code == 200
    data = rv1.data.decode()
    assert "Vous êtes déconnecté" in data
