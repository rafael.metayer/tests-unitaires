import pytest
from server import app, loadClubs, loadCompetitions

# commande : pytest --disable-warnings -v test_integration.py


# Initialisation du testeur
@pytest.fixture
def tester():
    app.config["TESTING"] = True
    with app.test_client() as tester:
        print('=> *** I am a fixture : WebApp was successfully created and the test was : ')
        yield tester


# Lancement des fonctions d'usage d'un usage correct
def test_integration(tester):
    loadClubs()
    loadCompetitions()
    response = tester.get("/")
    assert response.status_code == 200
    rv2 = tester.post("/showSummary", data=dict(email="john@simplylift.co"), follow_redirects=True)
    assert rv2.status_code == 200
    rv3 = tester.get("http://127.0.0.1:5000/book/Event/Iron%20Temple")
    assert rv3.status_code == 200
    rv4 = tester.post("/purchasePlaces", data=dict(competition="Event2", club="Iron Temple", places="1"),
                     follow_redirects=True)
    assert rv4.status_code == 200
    data = rv4.data.decode()
    print(data)
    assert """<li>
                    Event2 </br>
                    Date: 2024-10-22 13:30:00 </br>
                    Number of Places: 12 </br>
        """ in data
    rv5 = tester.get("/logout", follow_redirects=True)
    assert rv5.status_code == 200