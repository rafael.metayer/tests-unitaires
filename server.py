import json

from flask import Flask, render_template, request, redirect, flash, url_for
from datetime import datetime


def loadClubs():
    with open('clubs.json') as c:
        listOfClubs = json.load(c)['clubs']
        return listOfClubs


def loadCompetitions():
    with open('competitions.json') as comps:
        listOfCompetitions = json.load(comps)['competitions']
        nowCompetitions = []
        oldCompetitions = []
        date = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        for comp in listOfCompetitions:
            if comp['date'] > date:
                nowCompetitions.append(comp)
            else:
                oldCompetitions.append(comp)
        return nowCompetitions, oldCompetitions


app = Flask(__name__)
app.secret_key = 'something_special'

nowCompetitions, oldCompetitions = loadCompetitions()
clubs = loadClubs()


@app.route('/')
def index():
    flash("")
    return render_template('index.html')


@app.route('/showSummary', methods=['POST'])
def showSummary():
    try:
        club = [club for club in clubs if club['email'] == request.form['email']][0]
        return render_template('welcome.html', club=club, competitions=nowCompetitions, oldCompetitions=oldCompetitions)
    except:
        flash("Désolé, cet email n'a pas été trouvé")
        return redirect(url_for("index"))


@app.route('/book/<competition>/<club>')
def book(competition, club):
    prix = 3
    try:
        foundClub = [c for c in clubs if c['name'] == club][0]
        foundCompetition = [c for c in nowCompetitions if c['name'] == competition][0]
        if foundClub and foundCompetition:
            flash("Prix d'une place = " + str(prix) + " points")
            return render_template('booking.html', club=foundClub, competition=foundCompetition)
    except:
        flash("Lien incorrect, veuillez vous reconnecter")
        return redirect(url_for("index"))


@app.route('/purchasePlaces', methods=['POST'])
def purchasePlaces():
    prix = 3
    competition = [c for c in nowCompetitions if c['name'] == request.form['competition']][0]
    club = [c for c in clubs if c['name'] == request.form['club']][0]
    placesRequired = int(request.form['places'])
    if 12 > placesRequired > 0:
        if int(competition['numberOfPlaces']) >= placesRequired:
            if int(club['points']) / prix >= placesRequired:
                competition['numberOfPlaces'] = int(competition['numberOfPlaces']) - placesRequired
                club['points'] = int(club['points']) - (placesRequired * prix)
                flash('Great-booking complete!')
                return render_template('welcome.html', club=club, competitions=nowCompetitions,
                                       oldCompetitions=oldCompetitions)
            else:
                flash("Pas assez de points disponibles dans le club! Prix d'une place = " + str(prix) + " points")
                return render_template('booking.html', club=club, competition=competition)
        else:
            flash("Pas assez de places disponibles pour l'évènement! Prix d'une place = " + str(prix) + " points")

            return render_template('booking.html', club=club, competition=competition)
    else:
        flash("Nombre de places invalides ! Prix d'une place = " + str(prix) + " points")

        return render_template('booking.html', club=club, competition=competition)


# TODO: Add route for points display
@app.route('/tableau')
def tableau():
    flash("")
    return render_template('tableau.html', clubs=clubs)


@app.route('/logout')
def logout():
    flash("Vous êtes déconnecté")
    return redirect(url_for('index'))


if __name__ == "__main__":
    app.run(debug=True)
