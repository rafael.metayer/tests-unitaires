## Tests Unitaires Groupe 2

## Description
Projet visant à développer une web app en rajoutant des fonctionalité et en corrigeant les erreurs à l'aide de tests unitaires basé sur chacune des fonctionalités

## Membre
Rafael 
Basme
Lisa
Axel

## Utilisation

Récupérer le dossire
Effectuer les imports avec de requierements.txt

Pour lancer l'application il faut lancer le server.py
On peut tester toutes les fonctionalités à la suite en exécutant les scripts dans le dossier tests

Lancer le coverage (en ligne de commande) à la racine du projet pour avoir le pourcentage couverture du code de nos tests unitaires

--> coverage run -m pytest

--> coverage html


Lancer le locust à la racine du projet pour effectuer des tests de performance sur l'application pendant qu'elle tourne.

--> locust

